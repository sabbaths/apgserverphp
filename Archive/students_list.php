<?php

require('Database.php');

$database = new Database();
$database->connectDB();
$students = $database->getStudents();

echo json_encode([ 'students' => $students]);

?>