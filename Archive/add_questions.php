<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">



<?php

require('Database.php');

$database = new Database();
$database->connectDB();
$courses = $database->getCourses();
$subjects = $database->getSubjects();

echo "<div class='w3-container w3-green w3-center'>  ";
echo "<a href='list.php'>BACK TO LIST </a><br/><br/><br/>";
echo "
<label for='male'>Subject</label>
<select id = 'select_subject'>
";
	foreach($subjects as $subject) {
		//echo $subject[2];
		print "  <option value=".$subject[0].">".$subject[2]."</option>";
	}
echo "
</select><br/>
";


?>

<br/>
<label for="male">Question</label><br/>
<textarea id='taq' rows="3" cols="50">
</textarea>

<br/>
<label for="male">A</label><br/>
<textarea id='taa' rows="3" cols="50">
</textarea>

<br/>
<label for="male">B</label><br/>
<textarea id='tab' rows="3" cols="50">
</textarea>

<br/>
<label for="male">C</label><br/>
<textarea id='tac' rows="3" cols="50">
</textarea>

<br/>
<label for="male">D</label><br/>
<textarea id='tad' rows="3" cols="50">
</textarea>

<br/>
<label for="male">Answer</label>
<select id ='select_answer'>
  <option value="A">A</option>
  <option value="B">B</option>
  <option value="C">C</option>
  <option value="D">D</option>
</select> <br />
<button class="w3-btn w3-white w3-border w3-border-red w3-round-xlarge" onclick="loadDoc()">Save</button>
</div>


<script>
function myFunction() {
	//alert("save " + document.getElementById("select_subject").value);

	var subject_id = document.getElementById("select_subject").value;
	var question = document.getElementById("taq").value.toString();
	var choice_1 = document.getElementById("taa").value;
	var choice_2 = document.getElementById("tab").value;
	var choice_3 = document.getElementById("tac").value;
	var choice_4 = document.getElementById("tad").value;
	var answer = document.getElementById("select_answer").value;

	if(!question){
		alert("empty question"); return;
	}
	if(!choice_1){
		alert("empty A"); return;
	}
		if(!choice_2){
		alert("empty B"); return;
	}
		if(!choice_3){
		alert("empty C"); return;
	}
	if(!choice_4){
		alert("empty D"); return;
	}


}

function loadDoc() {
	var subject_id = document.getElementById("select_subject").value;
	var question = document.getElementById("taq").value.toString();
	var choice_1 = document.getElementById("taa").value;
	var choice_2 = document.getElementById("tab").value;
	var choice_3 = document.getElementById("tac").value;
	var choice_4 = document.getElementById("tad").value;
	var answer = document.getElementById("select_answer").value;


	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) {
	 	alert(this.responseText);
	 	document.getElementById("taq").value = "";
	 	document.getElementById("taa").value = "";
	 	document.getElementById("tab").value = "";
	 	document.getElementById("tac").value = "";
	 	document.getElementById("tad").value = "";
	}
	};

  	xhttp.open("POST", "add_question.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("subject_id="+subject_id+
		"&question="+question+
		"&choice_1="+choice_1+
		"&choice_2="+choice_2+
		"&choice_3="+choice_3+
		"&choice_4="+choice_4+
		"&answer="+answer);
}
</script>
