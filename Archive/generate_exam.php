<?php

require('Database.php');

$course_id = isset($_REQUEST['course_id']) ? $_REQUEST['course_id'] : null;
$subject_id = isset($_REQUEST['subject_id']) ? $_REQUEST['subject_id'] : null;
$student_id = isset($_REQUEST['student_id']) ? $_REQUEST['student_id'] : null;
$instructor_id = isset($_REQUEST['instructor_id']) ? $_REQUEST['instructor_id'] : null;
$user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : null;

if($course_id == null || $user_id == null 
        || $subject_id == null || $student_id == null 
        || $instructor_id == null) 
    exit("null items");

$database = new Database();
$database->connectDB();
$record = $database->generateExam($course_id, $subject_id, $student_id, $instructor_id, $user_id);

//$response_data = [ 'inserted_row_id' => $record];
echo json_encode($record);