<?php

require('Database.php');

$student_exam_id = isset($_REQUEST['student_exam_id']) ? $_REQUEST['student_exam_id'] : null;


if($student_exam_id == null) 
    exit("null items");

$database = new Database();
$database->connectDB();
$status_code = $database->getExamScore($student_exam_id);
//$response_data = [ 'status_code' => $status_code];

echo json_encode($status_code);