<?php


require('Database.php');

$subject_id = isset($_REQUEST['subject_id']) ? $_REQUEST['subject_id'] : null;
$question = isset($_REQUEST['question']) ? $_REQUEST['question'] : null;
$choice_1 = isset($_REQUEST['choice_1']) ? $_REQUEST['choice_1'] : null;
$choice_2 = isset($_REQUEST['choice_2']) ? $_REQUEST['choice_2'] : null;
$choice_3 = isset($_REQUEST['choice_3']) ? $_REQUEST['choice_3'] : null;
$choice_4 = isset($_REQUEST['choice_4']) ? $_REQUEST['choice_4'] : null;
$answer = isset($_REQUEST['answer']) ? $_REQUEST['answer'] : null;

$database = new Database();
$database->connectDB();
$status_code = $database->addSubjectQuestion($subject_id, $question, $choice_1,
		$choice_2, $choice_3, $choice_4, $answer);

$response = ["status_code" => $status_code];
echo json_encode($response);
?>