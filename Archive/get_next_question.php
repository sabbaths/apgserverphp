<?php

require('Database.php');

$student_exam_id = isset($_REQUEST['student_exam_id']) ? $_REQUEST['student_exam_id'] : null;

if($student_exam_id == null) exit("student exam id is null");

$database = new Database();
$database->connectDB();
$question = $database->getNextQuestion($student_exam_id);

$response = ["question" => $question];
//print_r($response);
echo json_encode($response);