<?php

include_once('Database.php');

$username = isset($_REQUEST['username']) ? $_REQUEST['username'] : "noun";
$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : "nopw";

//echo "INPUT:". $username . $password;

$database = new Database();
$database->connectDB();
$status_code = $database->login($username, $password);

$response_data = [ 'status_code' => $status_code[0], 'user' => $status_code[1]];
echo json_encode( $response_data );

?>