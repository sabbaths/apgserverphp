<?php


require('Database.php');

$subject_id = isset($_REQUEST['subject_id']) ? $_REQUEST['subject_id'] : null;
$code = isset($_REQUEST['code']) ? $_REQUEST['code'] : null;
$subject = isset($_REQUEST['subject']) ? $_REQUEST['subject'] : null;
$time = isset($_REQUEST['time']) ? $_REQUEST['time'] : null;
/*$choice_c = isset($_REQUEST['choice_3']) ? $_REQUEST['choice_3'] : null;
$choice_d = isset($_REQUEST['choice_4']) ? $_REQUEST['choice_4'] : null;
$answer = isset($_REQUEST['answer']) ? $_REQUEST['answer'] : null; */

$database = new Database();
$database->connectDB();
$status_code = $database->editSubject($subject_id, $code, $subject, $time);

$response = ["status_code" => $status_code];
echo json_encode($response);
?>