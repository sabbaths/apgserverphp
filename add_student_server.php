<?php

include_once('Database.php');

$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$first_name = isset($_REQUEST['first_name']) ? $_REQUEST['first_name'] : null;
$middle_name = isset($_REQUEST['middle_name']) ? $_REQUEST['middle_name'] : null;
$last_name = isset($_REQUEST['last_name']) ? $_REQUEST['last_name'] : null;

if($first_name == null || $last_name == null) 
    exit("null items");

$database = new Database();
$database->connectDB();
$status_code = $database->addStudent($first_name, $middle_name, $last_name, $id);

$response_data = [ 'status_code' => $status_code];
echo json_encode( $response_data );

?>