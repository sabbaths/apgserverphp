<?php

require('Database.php');

$course_id = isset($_REQUEST['course_id']) ? $_REQUEST['course_id'] : null;
$subject_id = isset($_REQUEST['subject_id']) ? $_REQUEST['subject_id'] : null;
$student_id = isset($_REQUEST['student_id']) ? $_REQUEST['student_id'] : null;
$instructor_id = isset($_REQUEST['instructor_id']) ? $_REQUEST['instructor_id'] : null;
$user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : null;
$student_exam_id = isset($_REQUEST['student_exam_id']) ? $_REQUEST['student_exam_id'] : null;
$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : null;

if($course_id == null || $user_id == null 
        || $subject_id == null || $student_id == null 
        || $instructor_id == null || $student_exam_id == null
        || $limit == null) 
    exit("null items");

$database = new Database();
$database->connectDB();
$status_code = $database->generateExamQuestions($course_id, $subject_id, 
        $student_id, $instructor_id, $student_exam_id, $user_id, $limit);

$response_data = [ 'status_code' => $status_code];
echo json_encode($response_data);