<?php

require('Database.php');

$database = new Database();
$database->connectDB();
$instructors = $database->getGI();

echo json_encode([ 'instructors' => $instructors]);

?>