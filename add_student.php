<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<script src="js/jquery.js"></script>

<body>
    
<div action="form.asp" class="w3-card-4"> 
  <div class="w3-container w3-green">
    <h2>Add Student</h2>

  </div>

  <form id='add-stud-form' class="w3-container" action="students.php" method='POST'>
    <div id='idnga' class="w3-panel w3-red"></div>
    <p>

    <label class="w3-label w3-text-brown"><b>ID:</b></label>
    <input class="w3-input w3-border w3-sand" id='username' name="username" type="text"></p>
    <p>
    <label class="w3-label w3-text-brown"><b>First Name:</b></label>
    <input class="w3-input w3-border w3-sand" id='first_name' name="first_name" type="text"></p>
    <p>

    <p>
    <label class="w3-label w3-text-brown"><b>Middle Name:</b></label>
    <input class="w3-input w3-border w3-sand" id='middle_name' name="middle_name" type="text"></p>
    <p>
    <p>
    <label class="w3-label w3-text-brown"><b>Last Name:</b></label>
    <input class="w3-input w3-border w3-sand" id='last_name' name="last_name" type="text"></p>
    <p>
    <button class="w3-btn w3-brown">Add Student</button></p>
    <a href='site/students.php'>BACK TO STUDENT LIST</a>
  </form>

</div>

</body>
</html>

<script>

$('#add-stud-form').submit(function(e) {
     var form = this;
     e.preventDefault();
    
    var first_name = $('#first_name').val();
    var middle_name = $('#middle_name').val();
    var last_name = $('#last_name').val();

    if(first_name.length == 0 || last_name.length == 0) {
        alert("fill up all items");
          $("#idnga").find('p').remove();
          $( "#idnga" ).append( "<p>Fill all items</p>" );
        return;
    }

    $.ajax({
    url: 'add_student_server.php',
    type: 'POST',
    data: $(this).serialize(), // it will serialize the form data
          dataType: 'html'
      })
      .done(function(data){
        var response = JSON.parse(data);
        var status_code = response.status_code;
        if(status_code == 7004) {
            alert("DATABASE ERROR");
        } else if (status_code == 7001) {
            alert("Adding Student Successful");
            form.submit();
        } 
      })
      .fail(function(){
        alert('Ajax Submit Failed ...'); 
        e.preventDefault(); 
      });
});

function startSession() {

}
</script>