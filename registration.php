<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<script src="js/jquery.js"></script>

<body>
    
<div action="form.asp" class="w3-card-4"> 
  <div class="w3-container w3-green">
    <h2>Registration</h2>

  </div>

  <form id='reg-form' class="w3-container" action="index.php" method='POST'>
    <div id='idnga' class="w3-panel w3-red"></div>
    <p>

    <label class="w3-label w3-text-brown"><b>Username:</b></label>
    <input class="w3-input w3-border w3-sand" id='username' name="username" type="text"></p>
    <p>
    <label class="w3-label w3-text-brown"><b>Password:</b></label>
    <input class="w3-input w3-border w3-sand" id='password' name="password" type="password"></p>
    <p>
    <p>
    <label class="w3-label w3-text-brown"><b>Confirm Password:</b></label>
    <input class="w3-input w3-border w3-sand" id='confirmpassword' name="confirmpassword" type="password"></p>
    <p>
    <p>
    <label class="w3-label w3-text-brown"><b>First Name:</b></label>
    <input class="w3-input w3-border w3-sand" id='first_name' name="first_name" type="text"></p>
    <p>
    <p>
    <label class="w3-label w3-text-brown"><b>Middle Name:</b></label>
    <input class="w3-input w3-border w3-sand" id='middle_name' name="middle_name" type="text"></p>
    <p>
    <p>
    <label class="w3-label w3-text-brown"><b>Last Name:</b></label>
    <input class="w3-input w3-border w3-sand" id='last_name' name="last_name" type="text"></p>
    <p>
    <button class="w3-btn w3-brown">Register</button></p>
    <a href='index.php'>BACK TO LOGIN</a>
  </form>

</div>

</body>
</html>

<script>

$('#reg-form').submit(function(e) {
     var form = this;
     e.preventDefault();
    
    var username = $('#username').val();
    var password = $('#password').val();
    var first_name = $('#first_name').val();
    var middle_name = $('#middle_name').val();
    var last_name = $('#last_name').val();
    var confirmpassword = $('#confirmpassword').val();

    if(username.length == 0 || password.length == 0 || confirmpassword.length == 0 
        || first_name.length == 0 || middle_name.length == 0 || last_name.length == 0) {
        alert("fill up all items");
          $("#idnga").find('p').remove();
          $( "#idnga" ).append( "<p>Fill all items</p>" );
        return;
    }

    if(password !== confirmpassword) {
        alert("Passwords does not match");
        return;
    }

    if(username.length < 5) {
        alert("username should not be less than 5");
        return;
    }

    if(password.length < 8) {
        alert("password should not be less than 8");
        return;
    }

    $.ajax({
    url: 'register.php',
    type: 'POST',
    data: $(this).serialize(), // it will serialize the form data
          dataType: 'html'
      })
      .done(function(data){
        var response = JSON.parse(data);
        var status_code = response.status_code;
        if(status_code == 5002) {
            alert("Username already exists");
        } else if (status_code == 5001) {
            alert("Registration Successful");
            form.submit();
        } else if (status_code == 5004) {
            alert("Database Error");
        }
      })
      .fail(function(){
        alert('Ajax Submit Failed ...'); 
        e.preventDefault(); 
      });
});

function startSession() {

}
</script>