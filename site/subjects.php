<link rel="stylesheet" href="../css/w3.css">
<link rel="stylesheet" href="../css/user.css">
<script src="../js/add_edit_subjects.js"></script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>

<?php

include_once('nav.php');
require('../Database.php');

$database = new Database();
$database->connectDB();




echo "<div class='w3-padding-large' id='main'>";
echo "<header class='w3-container w3-center w3-white' id='home'>";
echo "<a href='../add_student.php'>ADD SUBJECT</a><br/><br/>";
$subjects = $database->getSubjects();

echo "
		<table class='w3-table-all w3-tiny '>
		<tr>
		  <th>ID</th>
		  <th>Code</th>
		  <th>Subject</th>
		  <th>Time Limit(Minutes)</th>
		  <th>EDIT/DELETE</th>
		</tr>";

foreach($subjects as $subject) {
	$subject_id = $subject[0];
	$time_limit = $subject[3] / 60;

	echo "<tr>";
	print "<td>" . $subject_id. "</td>"; //id
	print "<td id='tdcode$subject_id'>" . $subject[1]. "</td>"; //first name
	print "<td id='tdname$subject_id'>" . $subject[2]. "</td>"; //middle
	print "<td id='tdtime$subject_id'>" . $time_limit . "</td>"; //last
	
	print "<td  id='tdbtn$subject_id'><button id='$subject_id' class='list_btn' onclick='editSubject(this.id)'>EDIT</button></td>";
	echo "</tr>";
}

echo "</table> 
	</header>
	</div>";
?>

