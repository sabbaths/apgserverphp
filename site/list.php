<?php 
session_start(); 

if(isset($_SESSION['username'])) {
} else if(isset($_POST['username'])) {
  $_SESSION['username']= $_POST['username'];
} else {
  echo "UNAUTHORIZED";
  exit();
}

$subject_id = isset($_REQUEST['subject_id']) ? $_REQUEST['subject_id'] : 0;

include_once('nav.php');
?>

<link rel="stylesheet" href="../css/w3.css">
<link rel="stylesheet" href="../css/user.css">
<script src="../js/add_question.js"></script>
<script src="../js/edit_question.js"></script>
<script src="../js/jquery.min.js"></script>
<script src="../js/jquery.js"></script>
<link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-indigo.css">



<div class="w3-padding-large" id="main">
  <!-- Header/Home -->
	<header class="w3-container w3-left w3-white" id="home">
		<?php
			require('../Database.php');

			$subject = "";
			$database = new Database();
			$database->connectDB();
			$courses = $database->getCourses();
			$subjects = $database->getSubjects();

			echo " <form class='w3-container' action='list.php' id='carform'>
			<label for='subject'>List of Examination questions for Subject: </label></br>
			<select class='w3-container' name='subject_id' id = 'select_subject' form='carform'>
			";
			  print "  <option selected='false' value=0>ALL SUBJECT</option>";
			  foreach($subjects as $subject) {
			    
			    //echo $subject[2];
			    if($subject_id == $subject[0]) {
			      //echo $subject[0];
			      print "  <option selected='selected' value=".$subject[0].">".$subject[2]."</option>";
			    } else {
			      print "  <option value=".$subject[0].">".$subject[2]."</option>"; 
			    }
			    
			  }
			echo "</select>";


			echo "
					<input type='submit'><br/><br/>
				</form>";

			$questions = $database->getAllQuestions($subject_id);

			echo "
			<table id='tbl_questions' class='w3-table-all w3-medium w3-border '>
			
			<tr>
			  <th>ID</th>
			  <th>Question</th>
			  <th>A</th>
			  <th>B</th>
			  <th>C</th>
			  <th>D</th>
			  <th>Answer</th>
			  <th>Edit</th>
			</tr>";
			
			echo "<tr id='tbl_questions_first_row'>";
			print "<td>0</td>";
			print "<td><textarea class='w3-input w3-border' rows='5' cols='20' id='taq'></textarea></td>";
			print "<td><textarea class='w3-input w3-border' rows='5' cols='20' id='taa'></textarea></td>";
			print "<td><textarea class='w3-input w3-border' rows='5' cols='20' id='tab'></textarea></td>";
			print "<td><textarea class='w3-input w3-border' rows='5' cols='20' id='tac'></textarea></td>";
			print "<td><textarea class='w3-input w3-border' rows='5' cols='20' id='tad'></textarea></td>";
			print "<td>
					<select id ='select_answer'>
					  <option value='A'>A</option>
					  <option value='B'>B</option>
					  <option value='C'>C</option>
					  <option value='D'>D</option>
					</select>
				</td>";
			print "<td><button onclick='addExamQuestion()'>ADD</button></td>";
			echo "</tr>"; 

			foreach($questions as $question) {
				echo "<tr class='w3-border'>";
				$question_id = $question['question_id'];
				print "<td class='w3-border' >" . $question_id . "</td>";
				print "<td class='w3-border' id='tdquestion$question_id'>" . $question['question']. "</td>";
				print "<td class='w3-border' id='tda$question_id'>" . $question['choice_1']. "</td>";
				print "<td class='w3-border' id='tdb$question_id'>" . $question['choice_2']. "</td>";
				print "<td class='w3-border' id='tdc$question_id'>" . $question['choice_3']. "</td>";
				print "<td class='w3-border' id='tdd$question_id'>" . $question['choice_4']. "</td>";
				print "<td class='w3-border' id='tdans$question_id'>" . $question['answer_letter']. "</td>";
				print "<td id='tdbtn$question_id'' ><button class='list_btn' id='$question_id' onclick='editExamQuestion(this.id)'>EDIT</button></td>";
				echo "</tr>";
			}

			echo "</table>";

		?>

	</header>
</div>


