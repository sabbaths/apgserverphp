<link rel="stylesheet" href="../css/w3.css">
<link rel="stylesheet" href="../css/user.css">
<?php

include_once('nav.php');
require('../Database.php');

$database = new Database();
$database->connectDB();




echo "<div class='w3-padding-large' id='main'>";
echo "<header class='w3-container w3-center w3-white' id='home'>";
echo "<a href='../add_student.php'>ADD STUDENT</a><br/><br/>";
$students = $database->getStudents();

echo "
		<table class='w3-table-all w3-tiny '>
		<tr>
		  <th>ID</th>
		  <th>First Name</th>
		  <th>Middle Name</th>
		  <th>Last Name</th>
		  <th>EDIT/DELETE</th>
		</tr>";

foreach($students as $student) {
	echo "<tr>";
	print "<td>" . $student[0]. "</td>"; //id
	print "<td>" . $student[1]. "</td>"; //first name
	print "<td>" . $student[2]. "</td>"; //middle
	print "<td>" . $student[3]. "</td>"; //last
	print "<td><button onclick='editStudent()'>EDIT</button></td>";
	echo "</tr>";
}

echo "</table> 
	</header>
	</div>";
?>

