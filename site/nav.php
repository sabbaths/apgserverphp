<div class="w3-sidebar w3-bar-block w3-collapse w3-card w3-animate-left w3-white" style="width:150px;" id="mySidebar">
  <button class="w3-bar-item w3-button w3-large w3-hide-large" onclick="w3_close()">Close &times;</button>
  <a href="list.php" class="w3-bar-item w3-hover-black w3-button">
    HOME
  </a>
  <a class="w3-bar-item w3-button w3-hover-black" href='students.php'>
    STUDENTS
  </a>
  <a class="w3-bar-item w3-button w3-hover-black" href='subjects.php'>
    SUBJECTS
  </a>
  <a class="w3-bar-item w3-button w3-hover-black" href='instructors.php'>
    INSTRUCTORS
  </a>
  <a class="w3-bar-item w3-button w3-hover-black" href='courses.php'>
    COURSE
  </a>
  <a class="w3-bar-item w3-button w3-hover-black" href='logout.php'>
  LOGOUT
  </a>
</div>

<div class="w3-main" >
<div class="w3-light-grey">
  <button class="w3-button w3-grey w3-xlarge w3-hide-large" onclick="w3_open()">&#9776;</button>
</div>