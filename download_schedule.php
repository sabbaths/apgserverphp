<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<script src="js/jquery.js"></script>
<body>

<div id="target">
  Click here
</div>
<div id="other">
  Trigger the handler
</div>
    
    
</body>
</html>

<script>

$( "#target" ).click(function() {
    $( "#other" ).load( "http://www.yahoo.com", function( response, status, xhr ) {
        console.log( response + status + xhr);
    });
});

$(document).ready(function(){
    $.ajax({
        url: 'http://www.google.com',
        dataType: 'jsonp',
        success: function(dataWeGotViaJsonp){
            var text = '';
            var len = dataWeGotViaJsonp.length;
            console.log(len);
        }
    });
})

</script>