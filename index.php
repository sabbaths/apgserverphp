<!DOCTYPE html>
<html>
<title>APG Online Exam</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/w3.css">
<link rel="stylesheet" href="css/user.css">
<script src="js/jquery.js"></script>

<body>

<div action="site/list.php" class="w3-card-4">
  <div class="w3-container w3-gray">
    <h2>Exam System</h2>
  </div>
  <form id="log-form" class="w3-container" action="site/list.php" method="post">
    <p>
    <label class="w3-label w3-text-brown"><b>Username:</b></label>
    <input class="w3-input w3-border w3-sand" id='username' name="username" type="text"></p>
    <p>
    <label class="w3-label w3-text-brown"><b>Password:</b></label>
    <input class="w3-input w3-border w3-sand" id='password' name="password" type="password"></p>
    <p>
    <button class="w3-btn w3-gray" >Login</button></p>
    <a class='w3-container' href='registration.php'>Register</a>
  </form>
</div>
<div class="w3-half w3-container w3-blue w3-border">  
  
  </div>

  <div id='idnga' class="w3-panel w3-red">
</div> 


</body>
</html>

<script>

$('#log-form').submit(function(e) {
     var form = this;
     e.preventDefault();
    
    var username = $('#username').val();
    var password = $('#password').val();

    if(username.length == 0 || password.length == 0) {
          $("#idnga").find('h1').remove();
          $( "#idnga" ).append( "<h1>DUH!!!</h1>" );
          return;
    }     

    $.ajax({
    url: 'login.php',
    type: 'POST',
    data: $(this).serialize(), // it will serialize the form data
          dataType: 'html'
      })
      .done(function(data){
        console.log("data");
        console.log(data);
        var response = JSON.parse(data);
        console.log("response");
        console.log(response);
        var status_code = response.status_code;
        if(status_code == 103) {
          $("#idnga").find('h1').remove();
          $( "#idnga" ).append( "<h1>Unregistered User</h1>" );
        } else if (status_code == 102) {
          $("#idnga").find('h1').remove();
          $( "#idnga" ).append( "<h1>Incorrect Password</h1>" );
        } else if (status_code == 101) {
            $.ajax({ url: 'session_start.php',
                     data: {action: 'calltofunction'},
                     type: 'POST',
                     success: function(output) {
                      form.submit();
                    }
            });
          //form.submit();
        }
      })
      .fail(function(){
        alert('Ajax Submit Failed ...'); 
        e.preventDefault(); 
      });
});

function startSession() {

}
</script>
