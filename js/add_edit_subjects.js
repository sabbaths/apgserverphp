function editSubject(subject_id) {
	console.log("subject" + subject_id);
	//var tdq = $('#tdquestion'+subject_id);
	var tdcode = $('#tdcode'+subject_id);
	var tdname = $('#tdname'+subject_id);
	var tdtime = $('#tdtime'+subject_id);
	//var tdd = $('#tdd'+subject_id);
	//var tdans = $('#tdans'+subject_id);
	var tdbtn = $('#tdbtn'+subject_id);
	var list_btn = $('.list_btn');
	list_btn.attr("disabled", "disabled");
	var tdq_html = tdcode.html();
	var tda_html = tdname.html();
	var tdb_html = tdtime.html();
	//var tdc_html = tdc.html();
	//var tdd_html = tdd.html();
	//var tdans_html = tdans.html();

	tdcode.html("<textarea id='ta_edit_ques"+subject_id+"' class='w3-input w3-border' id='edit_taques' rows='5' cols='25'>" + tdq_html + "</textarea>");
	tdname.html("<textarea id='ta_edit_a"+subject_id+"' class='w3-input w3-border' id='' rows='5' cols='25'>" + tda_html + "</textarea>");
	tdtime.html("<textarea id='ta_edit_b"+subject_id+"' class='w3-input w3-border' id='' rows='5' cols='25'>" + tdb_html + "</textarea>");
	//tdc.html("<textarea id='ta_edit_c"+subject_id+"' class='w3-input w3-border' id='' rows='5' cols='25'>" + tdc_html + "</textarea>");
	//tdd.html("<textarea id='ta_edit_d"+subject_id+"' class='w3-input w3-border' id='' rows='5' cols='25'>" + tdd_html + "</textarea>");
	/*tdans.html("<select id ='sa_edit_answer"+subject_id+"'>" +
				  "<option value='A'>A</option> " +
				  "<option value='B'>B</option> " +
				  "<option value='C'>C</option> " +
				  "<option value='D'>D</option> " +
				"</select>");
		*/
	tdbtn.html("<button onclick='saveEditedSubject("+subject_id+")'>SAVE</button>"); 
}

function saveEditedSubject(subject_id) {
	var code = $('#ta_edit_ques'+subject_id).val();
	var subj = $('#ta_edit_a'+subject_id).val();
	var time = $('#ta_edit_b'+subject_id).val();


	var tdq = $('#tdcode'+subject_id);
	var tda = $('#tdname'+subject_id);
	var tdb = $('#tdtime'+subject_id);
	var tdbtn = $('#tdbtn'+subject_id);
	var list_btn = $('.list_btn');
	
	tdq.html(code);
	tda.html(subj);
	tdb.html(time);

	tdbtn.html("<button class='list_btn' id='" + subject_id + "' onclick='editSubject(this.id)'>EDIT</button>");

	list_btn.prop('disabled', false);
	
	$.ajax({  
	    type: 'POST',  
	    url: '../edit_subject.php', 
	    data: { subject_id: subject_id,
	    	 	code: code,
	    		subject: subj,
	    		time: time*60,
	    },
	    success: function(response) {
	    	console.log(response);
	    	console.log("Edit question success");
	    },
	    error: function() {
	    	alert("Error: Contact Developer");
	    }
	}); 
}