function addExamQuestion() {
	var subject_id = document.getElementById("select_subject").value;
	var question = document.getElementById("taq").value.toString();
	var choice_1 = document.getElementById("taa").value;
	var choice_2 = document.getElementById("tab").value;
	var choice_3 = document.getElementById("tac").value;
	var choice_4 = document.getElementById("tad").value;
	var answer = document.getElementById("select_answer").value;



	
	if(!question){
		alert("Empty Question"); return;
	}
	if(!choice_1){
		alert("Empty A"); return;
	}
	if(!choice_2){
		alert("Empty B"); return;
	}
	if(!choice_3){
		alert("Empty C"); return;
	}
	if(!choice_4){
		alert("Empty D"); return;
	}

	
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
		 	//alert(this.responseText);
		 	var inserted_question_id = this.responseText;
		 	document.getElementById("taq").value = "";
		 	document.getElementById("taa").value = "";
		 	document.getElementById("tab").value = "";
		 	document.getElementById("tac").value = "";
		 	document.getElementById("tad").value = "";

			$('#tbl_questions_first_row').after('<tr>' +
				'<td><p>' + inserted_question_id + '</p></td>' +
				'<td><p>' + question + '</p></td>' +
				'<td><p>' + choice_1 + '</p></td>' +
				'<td><p>' + choice_2 + '</p></td>' +
				'<td><p>' + choice_3 + '</p></td>' +
				'<td><p>' + choice_4 + '</p></td>' +
				'<td><p>' + answer + '</p></td>' +
				'<td><p>' + '<button>EDIT</button>' + '</p></td>' +
				'</tr>');
		}
	};

  	xhttp.open("POST", "../add_question.php", true);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("subject_id="+subject_id+
				"&question="+question+
				"&choice_1="+choice_1+
				"&choice_2="+choice_2+
				"&choice_3="+choice_3+
				"&choice_4="+choice_4+
				"&answer="+answer);
	


}