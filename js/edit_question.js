function editExamQuestion(question_id) {
	var tdq = $('#tdquestion'+question_id);
	var tda = $('#tda'+question_id);
	var tdb = $('#tdb'+question_id);
	var tdc = $('#tdc'+question_id);
	var tdd = $('#tdd'+question_id);
	var tdans = $('#tdans'+question_id);
	var tdbtn = $('#tdbtn'+question_id);
	var list_btn = $('.list_btn');
	list_btn.attr("disabled", "disabled");
	var tdq_html = tdq.html();
	var tda_html = tda.html();
	var tdb_html = tdb.html();
	var tdc_html = tdc.html();
	var tdd_html = tdd.html();
	var tdans_html = tdans.html();

	tdq.html("<textarea id='ta_edit_ques"+question_id+"' class='w3-input w3-border' id='edit_taques' rows='5' cols='25'>" + tdq_html + "</textarea>");
	tda.html("<textarea id='ta_edit_a"+question_id+"' class='w3-input w3-border' id='' rows='5' cols='25'>" + tda_html + "</textarea>");
	tdb.html("<textarea id='ta_edit_b"+question_id+"' class='w3-input w3-border' id='' rows='5' cols='25'>" + tdb_html + "</textarea>");
	tdc.html("<textarea id='ta_edit_c"+question_id+"' class='w3-input w3-border' id='' rows='5' cols='25'>" + tdc_html + "</textarea>");
	tdd.html("<textarea id='ta_edit_d"+question_id+"' class='w3-input w3-border' id='' rows='5' cols='25'>" + tdd_html + "</textarea>");
	tdans.html("<select id ='sa_edit_answer"+question_id+"'>" +
				  "<option value='A'>A</option> " +
				  "<option value='B'>B</option> " +
				  "<option value='C'>C</option> " +
				  "<option value='D'>D</option> " +
				"</select>");
	
	tdbtn.html("<button onclick='saveEditedQuestion("+question_id+")'>SAVE</button>");
}

function saveEditedQuestion(question_id) {
	var q = $('#ta_edit_ques'+question_id).val();
	var a = $('#ta_edit_a'+question_id).val();
	var b = $('#ta_edit_b'+question_id).val();
	var c = $('#ta_edit_c'+question_id).val();
	var d = $('#ta_edit_d'+question_id).val();
	var ans = $('#sa_edit_answer'+question_id).val();

	var tdq = $('#tdquestion'+question_id);
	var tda = $('#tda'+question_id);
	var tdb = $('#tdb'+question_id);
	var tdc = $('#tdc'+question_id);
	var tdd = $('#tdd'+question_id);
	var tdans = $('#tdans'+question_id);
	var tdbtn = $('#tdbtn'+question_id);
	var list_btn = $('.list_btn');
	
	tdq.html(q);
	tda.html(a);
	tdb.html(b);
	tdc.html(c);
	tdd.html(d);
	tdans.html(ans);
	tdbtn.html("<button class='list_btn' id='" + question_id + "' onclick='editExamQuestion(this.id)'>EDIT</button>");

	list_btn.prop('disabled', false);

	$.ajax({  
	    type: 'POST',  
	    url: '../edit_subject_question.php', 
	    data: { question_id: question_id,
	    	 	question: q,
	    		choice_1: a,
	    		choice_2: b,
	    		choice_3: c,
	    		choice_4: d,
	    		answer: ans,
	    },
	    success: function(response) {
	    	console.log(response);
	    	console.log("Edit question success");
	    },
	    error: function() {
	    	alert("Error: Contact Developer");
	    }
	});
}