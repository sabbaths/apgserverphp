<!DOCTYPE html>
<html>
<title>W3.CSS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
<script src="js/jquery.js"></script>
<body>

<div class="w3-container">
  <h4>WEIGHT AND BALANCE</h4>
    <div class="w3-dropdown-hover">
      <p>AIRCRAFT:
        <select class="aircraft">
          <option value="SELECT AIRCRAFT">SELECT AIRCRAFT</option>
          <option value="2878">2878</option>
          <option value="2589">2589</option>
          <option value="3380">3380</option>
          <option value="2586">2586</option>
          <option value="2583">2583</option>
          <option value="INPUT">INPUT</option>
        </select>
      </div>
    </div>
  <table class="w3-table">
    <tr>
      <th></th>
      <th>Weight</th>
      <th>Arm</th>
      <th>Moment</th>
    </tr>
    <tr>
      <td></td>
      <td id="acweight"><input id="inputw" disabled="true" type="text" name="acw" size="10"></td>
      <td>0</td>
      <td id="acmoment"><input id="inputm" disabled="true" type="text" name="acm" size="10"></td>
    </tr>
    <tr>
      <td>FUEL</td>
      <td><input type="text" name="paxw" size="10"></td>
      <td>42</td>
      <td id="tdpaxm"></td>
    </tr>
    <tr>
      <td>PAX</td>
      <td><input type="text" name="fuelw" size="10"></td>
      <td>39</td>
      <td id="tdfuelm"></td>
    </tr>
        <tr>
      <td>BAG 1</td>
      <td><input type="text" name="bag1w" size="10"></td>
      <td>64</td>
      <td id="tdbag1m"></td>
    </tr>
    <tr>
      <td>BAG 2</td>
      <td><input type="text" name="bag2w" size="10"></td>
      <td>84</td>
      <td id="tdbag2m"></td>
    </tr>
    <tr>
      <td>TOTAL</td>
      <td id="tdtotalw">TOTAL WEIGHT</td>
      <td id="tdcg">CG</td>
      <td id="tdtotalm">TOTAL MOMENT</td>
    </tr>
  </table>
</div>

</body>
</html>

<script>
    
$( ".aircraft" ).change(function() {
  var selectedAircraft = $( ".aircraft option:selected" ).text();
  
  if(selectedAircraft == 2878) {
 
  } else if (selectedAircraft == 3380) {
    $('#inputw').val("1188.50");
    $('#inputm').val("32047.16");       
  } else if (selectedAircraft == 2586) {
    $('#inputw').val("1188.50");
    $('#inputm').val("32047.16"); 
  } else if (selectedAircraft == 2583) {
    $('#inputw').val("1188.50");
    $('#inputm').val("32047.16"); 
  } else if (selectedAircraft == 2589) {
    $('#inputw').val("1193.50");
    $('#inputm').val("30849.52");
  } else if (selectedAircraft == "INPUT") {
    $('#inputw').prop('disabled', false);
    $('#inputm').prop('disabled', false);
  }
  

});

$('input').change(function() {
    $('#tdtotalw').html("TOTAL WEIGHT");
    var inputName = $(this).attr('name');
    var inputValue = $(this).val();
    var moment = 0;
    var totalWeight = 0;
    var totalMoment = 0;
    var cg = 0;
    
    if(inputName == "paxw") {
        moment = inputValue * 42;
        $('#tdpaxm').html(moment); 
    } else if(inputName == "fuelw") {
        moment = inputValue * 39;
        $('#tdfuelm').html(moment); 
    } else if(inputName == "bag1w") {
        moment = inputValue * 64;
        $('#tdbag1m').html(moment); 
    } else if(inputName == "bag2w") {
        moment = inputValue * 84;
        $('#tdbag2m').html(moment); 
    }
    
    var acw  =parseInt($( "input[name='acw']" ).val()) || 0;
    var paxw  =parseInt($( "input[name='paxw']" ).val()) || 0;
    var fuelw  =parseInt($( "input[name='fuelw']" ).val()) || 0;
    var bag1w  =parseInt($( "input[name='bag1w']" ).val()) || 0;
    var bag2w  =parseInt($( "input[name='bag2w']" ).val()) || 0;
    totalWeight = acw + paxw + fuelw + bag1w + bag2w;

    var acm  =parseInt($( "input[name='acm']" ).val()) || 0;
    var paxm  =parseInt($("#tdpaxm").html()) || 0;
    var fuelm  =parseInt($("#tdfuelm").html()) || 0;
    var bag1m  =parseInt($("#tdbag1m").html()) || 0;
    var bag2m  =parseInt($("#tdbag2m").html()) || 0;
    totalMoment = acm + paxm + fuelm + bag1m + bag2m;
    
    $('#tdtotalw').html(totalWeight);
    $('#tdtotalm').html(totalMoment);
    
    var totalw = parseInt($("#tdtotalw").html()) || 0;
    var totalm = parseInt($("#tdtotalm").html()) || 0;
    $('#tdcg').html("CG: " + (totalm / totalw));
});

</script>