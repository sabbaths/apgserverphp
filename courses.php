<?php

require('Database.php');

$database = new Database();
$database->connectDB();
$courses = $database->getCourses();

echo json_encode([ 'courses' => $courses]);

?>