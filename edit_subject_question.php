<?php


require('Database.php');

$question_id = isset($_REQUEST['question_id']) ? $_REQUEST['question_id'] : null;
$question = isset($_REQUEST['question']) ? $_REQUEST['question'] : null;
$choice_a = isset($_REQUEST['choice_1']) ? $_REQUEST['choice_1'] : null;
$choice_b = isset($_REQUEST['choice_2']) ? $_REQUEST['choice_2'] : null;
$choice_c = isset($_REQUEST['choice_3']) ? $_REQUEST['choice_3'] : null;
$choice_d = isset($_REQUEST['choice_4']) ? $_REQUEST['choice_4'] : null;
$answer = isset($_REQUEST['answer']) ? $_REQUEST['answer'] : null;

$database = new Database();
$database->connectDB();
$status_code = $database->editSubjectQuestion($question_id, $question, $choice_a, $choice_b, $choice_c, $choice_d, $answer);

$response = ["status_code" => $status_code];
echo json_encode($response);
?>