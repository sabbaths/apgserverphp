<?php

require('Database.php');

$course_id = isset($_REQUEST['course_id']) ? $_REQUEST['course_id'] : "";

$database = new Database();
$database->connectDB();
$subjects = $database->getSubjects($course_id);

echo json_encode([ 'subjects' => $subjects]);

?>