<?php

require('Database.php');

$database = new Database();
$database->connectDB();
$students = $database->getStudents();
$json_encode = json_encode([ 'students' => $students]);
echo $json_encode;

?>