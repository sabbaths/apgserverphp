<?php

require('Database.php');

$exam_id = isset($_REQUEST['exam_id']) ? $_REQUEST['exam_id'] : null;
$is_correct = isset($_REQUEST['is_correct']) ? $_REQUEST['is_correct'] : null;

if($exam_id == null || $is_correct == null) exit("NULL");

$database = new Database();
$database->connectDB();
$status_code = $database->updateExamQuestion($exam_id, $is_correct);
$response = ["status_code" => $status_code];

echo json_encode($response);