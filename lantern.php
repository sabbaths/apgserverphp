<!DOCTYPE HTML>
<html>
  <script src="js/jquery.js"></script>
  <head>
    <style>
      body {
        margin: 100px;
        padding: 0px;
        background: black;
      }
    </style>
  </head>
  <body>
      
    <canvas id="myCanvas" width="578" height="200"></canvas>
    <canvas id="myCanvas2" width="578" height="200"></canvas>
    <textarea id="textareaColor" rows='10'></textarea>
    <button id="time" width="30" height="39">START</button>
    
    <script>
        var canvas = document.getElementById('myCanvas');
        var context = canvas.getContext('2d');

        context.fillStyle="#ffff00";//#FF0000"; //red
        context.fillRect(20,20,30,30); 

        var canvas2 = document.getElementById('myCanvas2');
        var context2 = canvas2.getContext('2d');

        context2.fillStyle="#00ff00"; //green
        context2.fillRect(20,20,30,30);   
    </script>
    <script>
        $("#time").click(function(){
            var canvas = document.getElementById('myCanvas');
            var context = canvas.getContext('2d');
            var canvas2 = document.getElementById('myCanvas2');
            var context2 = canvas2.getContext('2d');
            
            var fiveMinutes = 10,
            display = $('#time');
            var textArea = $('#textareaColor');
            
            var varCounter = 0;
            var varName = function(){
                 if(varCounter < 60) {
                    console.log(varCounter);
                     if(varCounter != 1 && 
                             varCounter != 10 && 
                             varCounter != 20 &&
                             varCounter != 30 &&
                             varCounter != 40 &&
                             varCounter != 50) {
                         varCounter++;
                         console.log("not equal");
                         return;
                     }
                     console.log("Here");
                      varCounter++;


                    /*
                       *  1 red FF0000
                       *  2 green 00ff00
                       *  3 yellow ffff00
                       */
                      var random1 = getRandomInt(1,3);  
                      var random2 = getRandomInt(1,3);  
                      var firstColor = "";
                      var secondColor = "";
                      
                      if(random1 == 1) {
                        context.fillStyle="#FF0000"; //red
                        context.fillRect(20,20,30,30); 
                        firstColor = "red";
                      } else if(random1 == 2) {
                        context.fillStyle="#00ff00"; //green
                        context.fillRect(20,20,30,30); 
                        firstColor = "green";
                      } else {
                        context.fillStyle="#ffff00"; //green
                        context.fillRect(20,20,30,30);   
                        firstColor = "yellow";
                      }
                      if(random2 == 1) {
                        context2.fillStyle="#FF0000"; //green
                        context2.fillRect(20,20,30,30); 
                        secondColor = "red";
                      } else if(random2 == 2) {
                        context2.fillStyle="#00ff00"; //green
                        context2.fillRect(20,20,30,30);   
                        secondColor = "green";
                      } else {
                        context2.fillStyle="#ffff00"; //green
                        context2.fillRect(20,20,30,30); 
                        secondColor = "yellow";
                      }
 
                      display.text("SECONDS: " + varCounter);
                      textArea.append(firstColor + " "+ secondColor + "\n");
                 } else {
                      clearInterval(varName);
                 }
            };
            setInterval(varName, 1000);
        });
        
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
        
        
        function startTimer(duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;

                display.text(minutes + ":" + seconds);

                if (--timer <= 0) {
                    alert("done");
                    return;
                    timer = duration;
                }
            }, 1000);
        }


    </script>
  </body>
</html>   